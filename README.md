How to run the application
==========================
Required software to build and run this project

1. [Java Development Kit 11](https://adoptopenjdk.net/?variant=openjdk11&jvmVariant=hotspot)
2. [Maven](https://maven.apache.org/download.cgi) (Optional)
3. [Payara](https://www.payara.fish/software/downloads) (Optional)

Some required packages when running on fresh Ubuntu 20.04
1. [Install Docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04)
2. sudo apt install openjdk-11-jdk

Below describes several ways to build and run the application. 

Start with maven
----------------
1. Build & run: 
   - ./mvnw package payara-micro:start
   - Context is http://localhost:8080

Create bundle and start with java (Jakarta EE - Boot)
-------------------------------
1. Build: .
   - ./mvnw payara-micro:bundle 
2. Run: 
   - java -jar target/chat-microbundle.jar
   - Context is http://localhost:8080/

Create war and deploy in Payara server:
---------------------------------------
1. Build: 
   * ./mvnw package 
2. Run: 
   - Deploy target/chat.war in Payara server
   - Context is http://localhost:8080/
    
Create war & start with Docker
--------------------------------------------
1. Build:
   * ./mvnw clean package
2. docker run -d -p 8080:8080 --name chat -v ${PWD}/target:/opt/payara/deployments payara/micro:jdk11 --deploy /opt/payara/deployments/chat.war --contextroot ROOT --nocluster
    - Context is http://localhost:8080/



References
==========

Articles
--------
[MicroProfile JSON Web Token (JWT)](https://www.tomitribe.com/blog/microprofile-json-web-token-jwt)