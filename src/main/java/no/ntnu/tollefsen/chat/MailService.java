package no.ntnu.tollefsen.chat;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Singleton;
import javax.enterprise.event.ObservesAsync;
import javax.inject.Inject;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import lombok.extern.java.Log;
import no.ntnu.tollefsen.chat.domain.Message;
import org.eclipse.microprofile.config.inject.ConfigProperty;

/**
 *
 * @author mikael
 */
@Log
@Singleton
public class MailService {
    @Inject
    @ConfigProperty(name = "mail.smtp.host")
    String smtpHost;
    
    @Inject
    @ConfigProperty(name = "mail.smtp.username")
    String smtpUser;

    @Inject
    @ConfigProperty(name = "mail.smtp.password")
    String smtpPassword;
    
    
    /**
     * Send an email
     * 
     * @param message received event
     */    
    public void onAsyncMessage(@ObservesAsync Message message) {
        try {
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");  
            props.put("mail.smtp.host", smtpHost);
            props.put("mail.smtp.port", "587");
            Session mailSession = Session.getInstance(props, new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpUser, smtpPassword);
                }
            });

            MimeMessage mimeMessage = new MimeMessage(mailSession);
            mimeMessage.setSubject("New Message");
            
            String to = message.getConversation().getOwner().getEmail();
            if(to != null && to.length() > 0) {
                mimeMessage.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(to));
                mimeMessage.setFrom(new InternetAddress(smtpUser));
                mimeMessage.setText(message.getText());
                Transport.send(mimeMessage);
            } else {
                log.log(Level.INFO, "Failed to find email for user {0}", message.getConversation().getOwner().getUserid());
            }
        } catch (MessagingException ex) {
            Logger.getLogger(MailService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
