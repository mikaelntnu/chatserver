package no.ntnu.tollefsen.chat.domain;

import javax.ejb.EJB;
import javax.json.bind.adapter.JsonbAdapter;
import no.ntnu.tollefsen.chat.ConversationService;

/**
 *
 * @author mikael
 */
public class ConversationAdapter implements JsonbAdapter<Conversation, Long> {

    @EJB
    ConversationService service;
    
    @Override
    public Long adaptToJson(Conversation conv) throws Exception {        
        return conv != null ? conv.getId() : null;
    }

    @Override
    public Conversation adaptFromJson(Long id) throws Exception {
        return service.findById(id);
    }
    
}
